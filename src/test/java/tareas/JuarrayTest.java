package tareas;

import JUarray.JUArrayList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JuarrayTest {
    @Test
    void Array(){
        JUArrayList array = new JUArrayList();

        assertEquals(0,array.size());

        assertEquals(true,array.isEmpty());

        array.add(2);
        array.add(3);
        array.add(5);
        array.add(6);
        array.add(7);

        assertEquals(true,array.contains(5));

        array.remove(0); // por indice  ----- queda [3,5,6,7]

        assertEquals(false,array.contains(2));

        array.removeObject(6); // por objeto ------ queda [3,5,7]

        assertEquals(false,array.contains(6));

        assertEquals(5,array.get(1));

        array.clear();

        assertEquals(0,array.size());

        array.add(1);
        array.add(2);
        array.add(3);


    }
}
