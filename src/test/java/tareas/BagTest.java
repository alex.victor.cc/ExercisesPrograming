package tareas;


import Bag.bags.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class BagTest {

    @Test
    void selectionSort(){
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(29);
        bag.add(51);
        bag.add(35);
        bag.add(40);
        bag.add(10);
        assertEquals("10,40,35,51,29,null",bag.toString());
        bag.selectionSort();
        assertEquals("10,29,35,40,51,null",bag.toString());

        Bag<Integer> bag2 = new LinkedBag<>();
        bag2.add(-1);
        bag2.add(0);
        bag2.add(-2);
        bag2.add(0);
        bag2.add(-1);
        assertEquals("-1,0,-2,0,-1,null",bag2.toString());
        bag2.selectionSort();
        assertEquals("-2,-1,-1,0,0,null",bag2.toString());
    }

    @Test
    void bubbleSort(){
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(29);
        bag.add(51);
        bag.add(35);
        bag.add(40);
        bag.add(10);
        assertEquals("10,40,35,51,29,null",bag.toString());
        bag.bubbleSort();
        assertEquals("10,29,35,40,51,null",bag.toString());

        Bag<Integer> bag2 = new LinkedBag<>();
        bag2.add(-1);
        bag2.add(0);
        bag2.add(-2);
        bag2.add(0);
        bag2.add(-1);
        assertEquals("-1,0,-2,0,-1,null",bag2.toString());
        bag2.bubbleSort();
        assertEquals("-2,-1,-1,0,0,null",bag2.toString());
    }
}
