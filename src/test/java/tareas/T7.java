package tareas;

import T7.Node;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class T7 {

    @Test
    void getNth() {
        //situacion normal
        Node<Integer> node1 = new Node<>(1,new Node<>(2,new Node<>(3)));
        assertEquals(1,node1.getNth(node1,0));
        assertEquals(2,node1.getNth(node1,1));
        assertEquals(3,node1.getNth(node1,2));

        // index sobrepasa los limites
        assertNull(node1.getNth(node1, 3));
        assertNull(node1.getNth(node1, 6));
        assertNull(node1.getNth(node1, 10));

        //Lista vacia
        Node<Integer> vacio = null;
        assertNull(node1.getNth(vacio,0));
        assertNull(node1.getNth(null,10));
    }

    @Test
    void append() {
        //Normal
        Node<Integer> node1 = new Node<>(1,new Node<>(2,new Node<>(3)));
        Node<Integer> node2 = new Node<>(4,new Node<>(5,new Node<>(6)));

        Node<Integer> nodExpected = node1.append(node1,node2);
        String expected = "123456null";

        assertEquals(expected,nodExpected.toString());

        assertEquals(1,nodExpected.getNth(nodExpected,0));
        assertEquals(3,nodExpected.getNth(nodExpected,2));
        assertEquals(4,nodExpected.getNth(nodExpected,3));
        assertEquals(6,nodExpected.getNth(nodExpected,5));

        // Verificamos cuantos elementos tiene
        int size = 0;
        for(Node<Integer> nod = nodExpected; nod != null; nod = nod.next){
            size++;
        }
        assertEquals(6,size);

        // el primero null , deberia de retornar el segundo
        Node<Integer> firsNull = new Node<>(10,new Node<>(12,new Node<>(15)));
        assertEquals(firsNull,firsNull.append(null,firsNull));

        // el segundo null , deveria de retornar el primero
        Node<Integer> secondNull = new Node<>(100,new Node<>(200,new Node<>(300)));
        assertEquals(secondNull,secondNull.append(secondNull,null));

        // ambos nulos
        assertNull(node1.append(null,null));
    }

}
