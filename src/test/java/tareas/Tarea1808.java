package tareas;
import InterfaceList.*;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

public class Tarea1808 {
    @Test
    void isEmpty(){
        List<Integer> list = new CircularLinkedList<>();
        // lista vacia
        assertTrue(list.isEmpty());
        // si se agregan elementos deberia de estar vacia
        list.add(1);
        list.add(2);
        assertFalse(list.isEmpty());

        // si se quitan todos los elementos deberia de estar vacia
        list.remove(1);
        list.remove(2);
        assertTrue(list.isEmpty());

        List<String> list2 = new CircularLinkedList<>();
        // lista vacia
        assertTrue(list2.isEmpty());
        // si se agregan elementos deberia de estar vacia
        list2.add("1");
        list2.add("2");
        assertFalse(list2.isEmpty());

        // si se quitan todos los elementos deberia de estar vacia
        list2.remove("1");
        list2.remove("2");
        assertTrue(list2.isEmpty());
    }

    @Test
    void size(){
        List<Integer> list = new CircularLinkedList<>();
        // la lista esta vacia , deveria de tener 0 elementos
        assertTrue(list.isEmpty());
        assertEquals(0,list.size());
        //si se añade un elemento deberia aumentar en uno
        list.add(1);
        assertEquals(1,list.size());
        list.add(2);
        assertEquals(2,list.size());
        // cuando se quita un elemento se deveria de reducir el size
        list.remove(2);
        assertEquals(1,list.size());
        // con valor nulo
        assertFalse(list.add(null));
        assertEquals(1,list.size());

        List<String> list2 = new CircularLinkedList<>();
        // la lista esta vacia , deveria de tener 0 elementos
        assertTrue(list2.isEmpty());
        assertEquals(0,list2.size());
        //si se añade un elemento deberia aumentar en uno
        list2.add("1");
        assertEquals(1,list2.size());
        list2.add("2");
        assertEquals(2,list2.size());
        // cuando se quita un elemento se deveria de reducir el size
        list2.remove("2");
        assertEquals(1,list2.size());
        // con valor nulo
        assertFalse(list2.add(null));
        assertEquals(1,list2.size());
    }

    @Test
    void add(){
        List<Integer> list = new CircularLinkedList<>();
        // en una lista vacia
        assertTrue(list.isEmpty());
        assertEquals(0,list.size());
        // Cuando se agrega uno se aumenta el size y ya no esta vacio
        list.add(1);  //1,
        assertFalse(list.isEmpty());
        assertEquals(1, list.size());
        assertEquals(1,list.get(0));
        // como es circular si el incie execede la longitud de la lista esta vuelve a emprezar desde la raiz
        assertEquals(1,list.get(4));
        //1,2,
        list.add(2);
        assertEquals(2, list.size());
        assertEquals(2, list.get(1));
        assertEquals(2,list.get(5));
        list.add(3);
        //1,2,3,
        assertEquals(3, list.size());
        assertEquals(1, list.get(3));
        list.add(4);
        //1,2,3,4,
        assertEquals(4, list.size());
        assertEquals(4, list.get(3));
        assertEquals(1, list.get(4));
        // Con un valor existente
        list.add(4);
        //1,2,3,4,4,
        assertEquals(5, list.size());
        assertEquals(4, list.get(4));
        assertEquals(1, list.get(5));

        // No permite valores nulos
        assertFalse(list.add(null));
        assertEquals(5, list.size());
        assertEquals(1,list.get(5));

        //probando con valor negativo
        list.add(-1);
        assertEquals(6, list.size());
        assertEquals(-1,list.get(5));

        List<String> list2 = new CircularLinkedList<>();
        // en una lista vacia
        assertTrue(list2.isEmpty());
        assertEquals(0,list2.size());
        // Cuando se agrega uno se aumenta el size y ya no esta vacio
        list2.add("1");
        assertFalse(list2.isEmpty());
        assertEquals(1, list2.size());
        assertEquals("1",list2.get(0));
        // como es circular si el incie execede la longitud de la lista esta vuelve a emprezar desde la raiz
        assertEquals("1",list2.get(4));

        //probando valor repetido
        list2.add("1");
        assertEquals(2,list2.size());
        assertEquals("1",list2.get(1));

        //valor diferente
        list2.add("2");
        assertEquals(3,list2.size());
        assertEquals("2",list2.get(2));
        assertEquals("1",list2.get(3));

        // valor nulo
        assertFalse(list2.add(null));
        assertEquals(3,list2.size());
    }

    @Test
    void remove(){
        List<Integer> list = new CircularLinkedList<>();
        // remover de una lista vacia no deveria poderse
        assertFalse(list.remove(1));
        assertFalse(list.remove(2));

        list.add(1);
        list.add(1);
        list.add(-2);
        assertEquals(3,list.size());

        assertTrue(list.remove(1));
        assertEquals(2,list.size());

        assertTrue(list.remove(1));
        assertEquals(1,list.size());

        assertTrue(list.remove(-2));
        assertEquals(0,list.size());
        // provando con valores no existentes

        assertFalse(list.remove(50));
        assertFalse(list.remove(100));

        // valor recien ingresado, la lista deveria de estar vacia cuando se elimino el ultimo elemento
        list.add(23);

        assertFalse(list.isEmpty());
        assertTrue(list.remove(23));
        assertTrue(list.isEmpty());

        List<String> list2 = new CircularLinkedList<>();
        // lista vacia
        assertFalse(list2.remove("1"));
        assertFalse(list2.remove("2"));

        list2.add("1");
        list2.add("2");
        list2.add("3");
        assertEquals(3,list2.size());

        assertTrue(list2.remove("1"));
        assertEquals(2,list2.size());

        assertTrue(list2.remove("2"));
        assertEquals(1,list2.size());

        assertTrue(list2.remove("3"));
        assertEquals(0,list2.size());
        // provando con valores no existentes

        assertFalse(list2.remove("hola"));
        assertFalse(list2.remove("oaimdmksfm"));

        // valor recien ingresado, la lista deveria de estar vacia cuando se elimino el ultimo elemento
        list2.add("23");

        assertFalse(list2.isEmpty());
        assertTrue(list2.remove("23"));
        assertTrue(list2.isEmpty());
    }

    @Test
    void get(){
        List<Integer> list = new CircularLinkedList<>();
        //cuando la lista esta vacia
        assertNull(list.get(0));
        assertNull(list.get(1));

        list.add(1);
        list.add(2);
        list.add(3);

        // con valor negativo
        assertNull(list.get(-1));

        //situacion normal
        assertEquals(1,list.get(0));
        assertEquals(2,list.get(1));
        assertEquals(3,list.get(2));

        //cuando el indice sobrepasa el size
        assertEquals(1,list.get(3));
        assertEquals(2,list.get(4));
        assertEquals(3,list.get(5));

        List<String> list2 = new CircularLinkedList<>();
        //cuando la lista esta vacia
        assertNull(list2.get(0));
        assertNull(list2.get(1));

        list2.add("hola");
        list2.add("Hello");
        list2.add("hi");

        // con valor negativo
        assertNull(list2.get(-1));

        //situacion normal
        assertEquals("hola",list2.get(0));
        assertEquals("Hello",list2.get(1));
        assertEquals("hi",list2.get(2));

        //cuando el indice sobrepasa el size
        assertEquals("hola",list2.get(3));
        assertEquals("Hello",list2.get(4));
        assertEquals("hi",list2.get(5));
    }

    @Test
    void bubbleSort(){
        List<Integer> list = new CircularLinkedList<>();
        // no hace nada con lista vacia
        list.bubbleSort();

        list.add(3);
        list.add(9);
        list.add(2);
        list.add(8);
        list.add(1);

        System.out.println("------BubbleSort Int-------");
        System.out.println(list);
        list.bubbleSort();
        System.out.println(list);
        System.out.println("-----Long list  BubbleSort Int------");
        int i = 6000;
        List<Integer> listLarge = CircularLinkedList.dummyList(i); // 60000->59999->59998->etc
        Instant inst1 = Instant.now();
        listLarge.bubbleSort();
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time by " + i +": " + Duration.between(inst1,inst2).toMillis());
        System.out.println("----------------------------");

        List<String> list2 = new CircularLinkedList<>();
        // no hace nada con la lista vacia
        list2.bubbleSort();

        list2.add("a");
        list2.add("f");
        list2.add("b");
        list2.add("ba");
        list2.add("c");
        list2.add("ah");

        System.out.println("------BubbleSort String-------");
        System.out.println(list2);
        list2.bubbleSort();
        System.out.println(list2);
        System.out.println("----------------------------");
    }

    @Test
    void selectionSort(){
        List<Integer> list = new CircularLinkedList<>();
        // no hace nada con lista vacia
        list.bubbleSort();

        list.add(10);
        list.add(19);
        list.add(28);
        list.add(17);
        list.add(100);
        list.add(200);
        list.add(192);
        list.add(299);
        list.add(15);
        list.add(6);
        list.add(92);
        list.add(1);
        list.add(59);

        System.out.println("------SelectionSort Int-------");
        System.out.println(list);
        list.bubbleSort();
        System.out.println(list);
        System.out.println("----------------------------");

        List<String> list2 = new CircularLinkedList<>();
        // no hace nada con la lista vacia
        list2.bubbleSort();

        list2.add("abeja");
        list2.add("fuego");
        list2.add("pelota");
        list2.add("bebe");
        list2.add("casa");
        list2.add("ahora");

        System.out.println("------SelectionSort String-------");
        System.out.println(list2);
        list2.bubbleSort();
        System.out.println(list2);
        System.out.println("----------------------------");
    }

}
