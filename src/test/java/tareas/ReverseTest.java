package tareas;

import InterfaceList.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ReverseTest {

    @Test
    void reverse(){
        List<Integer> listVoid = new CircularLinkedList<>();
        // en una lista vacia no deveria de hacer nada
        listVoid.reverse();
        assertTrue(listVoid.isEmpty());
        assertEquals(0,listVoid.size());


        List<Integer> listOne = new CircularLinkedList<>();
        // cuando solo hay un obejto en la lista
        listOne.add(10);
        assertEquals(10,listOne.get(0));
        assertEquals(10,listOne.get(1));
        assertEquals(1,listOne.size());

        listOne.reverse();

        assertEquals(10,listOne.get(0));
        assertEquals(10,listOne.get(1));
        assertEquals(1,listOne.size());


        List<Integer> listTwo = new CircularLinkedList<>();
        // con dos elementos en la lista
        listTwo.add(111);
        listTwo.add(222);

        assertEquals(111,listTwo.get(0));
        assertEquals(222,listTwo.get(1));
        assertEquals(111,listTwo.get(2));
        assertEquals(222,listTwo.get(3));
        assertEquals("(2) head = { data = 111, next ->{ data = 222, next -> head }}",listTwo.toString());

        listTwo.reverse();

        assertEquals("(2) head = { data = 222, next ->{ data = 111, next -> head }}",listTwo.toString());
        assertEquals(222,listTwo.get(0));
        assertEquals(111,listTwo.get(1));
        assertEquals(222,listTwo.get(2));
        assertEquals(111,listTwo.get(3));


        List<Integer> list = new CircularLinkedList<>();

        list.add(0);
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);

        assertEquals(0,list.get(0));
        assertEquals(20,list.get(2));
        assertEquals(40,list.get(4));
        assertEquals(50,list.get(5));
        assertEquals("(6) head = { data = 0, next ->{ data = 10, next ->{ data = 20, next ->{ " +
                              "data = 30, next ->{ data = 40, next ->{ data = 50, next -> head }}}}}}",
                              list.toString());

        list.reverse();


        assertEquals("(6) head = { data = 50, next ->{ data = 40, next ->{ data = 30, next ->{ " +
                              "data = 20, next ->{ data = 10, next ->{ data = 0, next -> head }}}}}}",
                              list.toString());
        assertEquals(50,list.get(0));
        assertEquals(30,list.get(2));
        assertEquals(10,list.get(4));
        assertEquals(0,list.get(5));


        // STRING


        List<String> listStringVoid = new CircularLinkedList<>();
        // en una lista vacia no deveria de hacer nada
        listStringVoid.reverse();
        assertTrue(listStringVoid.isEmpty());
        assertEquals(0,listStringVoid.size());


        List<String> listStringOne = new CircularLinkedList<>();
        // cuando solo hay un obejto en la lista
        listStringOne.add("A");
        assertEquals("A",listStringOne.get(0));
        assertEquals("A",listStringOne.get(1));
        assertEquals(1,listStringOne.size());

        listStringOne.reverse();

        assertEquals("A",listStringOne.get(0));
        assertEquals("A",listStringOne.get(1));
        assertEquals(1,listStringOne.size());


        List<String> listStringTwo = new CircularLinkedList<>();
        // con dos elementos en la lista
        listStringTwo.add("jjj");
        listStringTwo.add("===");

        assertEquals("jjj",listStringTwo.get(0));
        assertEquals("===",listStringTwo.get(1));
        assertEquals("jjj",listStringTwo.get(2));
        assertEquals("===",listStringTwo.get(3));
        assertEquals("(2) head = { data = jjj, next ->{ data = ===, next -> head }}",listStringTwo.toString());

        listStringTwo.reverse();

        assertEquals("(2) head = { data = ===, next ->{ data = jjj, next -> head }}",listStringTwo.toString());
        assertEquals("===",listStringTwo.get(0));
        assertEquals("jjj",listStringTwo.get(1));
        assertEquals("===",listStringTwo.get(2));
        assertEquals("jjj",listStringTwo.get(3));


        List<String> listString = new CircularLinkedList<>();

        listString.add("A");
        listString.add("B");
        listString.add("C");
        listString.add("D");
        listString.add("E");
        listString.add("F");

        assertEquals("A",listString.get(0));
        assertEquals("C",listString.get(2));
        assertEquals("E",listString.get(4));
        assertEquals("F",listString.get(5));
        assertEquals("(6) head = { data = A, next ->{ data = B, next ->{ data = C, next ->{ " +
                        "data = D, next ->{ data = E, next ->{ data = F, next -> head }}}}}}",
                listString.toString());

        listString.reverse();


        assertEquals("(6) head = { data = F, next ->{ data = E, next ->{ data = D, next ->{ " +
                        "data = C, next ->{ data = B, next ->{ data = A, next -> head }}}}}}",
                listString.toString());
        assertEquals("F",listString.get(0));
        assertEquals("D",listString.get(2));
        assertEquals("B",listString.get(4));
        assertEquals("A",listString.get(5));

    }

}
