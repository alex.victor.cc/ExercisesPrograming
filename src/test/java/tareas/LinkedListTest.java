package tareas;

import JULinkedList.LinkedList;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.sql.ClientInfoStatus;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class LinkedListTest {
    @Test
    void addTest(){
        LinkedList<Integer> list = new LinkedList<>();

        assertTrue(list.add(1));
        assertTrue(list.add(-2));

        assertTrue(list.contains(1));
        assertTrue(list.contains(-2));

        assertTrue(list.add(3));
        assertEquals(3, list.get(2));
        assertEquals(3,list.size());

        assertTrue(list.add(3));
        assertEquals(4,list.size());  //acepta repetido

        assertTrue(list.add(null));         //acepta null
        assertNull(list.get(4));

        LinkedList<String> list2 = new LinkedList<>();

        assertTrue(list2.add("1"));
        assertTrue(list2.contains("1"));

        assertTrue(list2.add(""));
        assertTrue(list2.contains(""));

        assertTrue(list2.add("Hola"));
        assertEquals("Hola", list2.get(2));
        assertEquals(3,list2.size());
        assertTrue(list2.add("Hola"));                 // acepta repetidos

    }

    @Test
    void isEmptyTest(){
        LinkedList<Integer> empty = new LinkedList<>();

        assertTrue(empty.isEmpty());          // lista resien creada

        empty.add(1);
        assertFalse(empty.isEmpty());         // lista con contenido

        empty.clear();
        assertTrue(empty.isEmpty());          //lista resien vaciada

        LinkedList<String> vacioString = new LinkedList<>();
        assertTrue(vacioString.isEmpty());    // lista resien creada

        vacioString.add("ya no es vacio");
        assertFalse(vacioString.isEmpty());  // con contenido

        vacioString.remove("ya no es vacio");    //eliminamos su unico elemento
        assertTrue(vacioString.isEmpty());        //resien vaciada
    }

    @Test
    void sizeTest(){
        List<Integer> list = new LinkedList<>();
        assertEquals(0,list.size());
        list.add(1);
        list.add(2);
        list.add(3);
        assertEquals(3,list.size());       // despues de añadir
        assertNotEquals(5,list.size());

        assertEquals(3,list.remove(2));        // despues de remover un elemento
        assertEquals(2,list.size());

        list.clear();
        assertEquals(0,list.size());        // despues de vaciar


        List<String> listString = new LinkedList<>();
        assertEquals(0,listString.size());   // lista vacia
        listString.add("hello");
        listString.add("world");
        listString.add("error");

        assertEquals(3,listString.size());
        assertNotEquals(5,listString.size());

        assertEquals("hello",listString.remove(0));
        assertEquals(2,listString.size());

        listString.clear();
        assertEquals(0,listString.size());
    }

    @Test
    void clearTest(){
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(-2);
        list.add(430);
        list.add(324225);

        assertFalse(list.isEmpty());

        list.clear();

        assertTrue(list.isEmpty());
        assertNull(list.get(0));

        List<String> empty = new LinkedList<>();
        empty.clear();                                          //borrando una lista vacia
        assertTrue(empty.isEmpty());

        empty.add("Hola");
        empty.add("Mundo");
        empty.add("Crue");
        assertFalse(empty.isEmpty());

        empty.clear();

        assertTrue(empty.isEmpty());
        assertNull(empty.get(2));
    }

    @Test
    void containsTest(){
        List<Integer> list = new LinkedList<>();
        assertFalse(list.contains(19));
        assertFalse(list.contains(5));
        list.add(12);
        list.add(19);
        list.add(5);

        assertTrue(list.contains(19));
        assertTrue(list.contains(5));

        assertFalse(list.contains(10));
        assertFalse(list.contains(-3));

        assertFalse(list.contains("hola"));
        assertFalse(list.contains("chau"));

        List<String> list2 = new LinkedList<>();

        assertFalse(list2.contains("hola"));     // lista vacia
        assertFalse(list2.contains("Word"));

        list2.add("hola");
        list2.add("Word");
        list2.add("     ");

        assertTrue(list2.contains("hola"));     // situacion normal
        assertTrue(list2.contains("Word"));

        assertFalse(list2.contains("nose"));     // no contiene
        assertFalse(list2.contains("almuerzo"));

        assertFalse(list2.contains(1));
        assertFalse(list2.contains(18));
    }

    @Test
    void getTest(){
        List<Integer> list = new LinkedList<>();
        list.add(1); // index = 0
        list.add(2); //         1
        list.add(3); //         2

        assertEquals(1,list.get(0));
        assertEquals(2,list.get(1));
        assertEquals(3,list.get(2));

        assertNotEquals(1,list.get(1));    //indice equivocado
        assertNotEquals(2,list.get(2));

        assertNull(list.get(3));         // el index sobrepasa la lista
        assertNull(list.get(6));

        assertNull(list.get(-2));       // numeros negativos
        assertNull(list.get(-7));

        List<String> list2 = new LinkedList<>();
        assertNull(list2.get(0));    //lista vacia
        assertNull(list2.get(1));

        list2.add("hola");
        list2.add("chau");
        list2.add("lol");

        assertEquals("hola",list2.get(0));
        assertEquals("chau",list2.get(1));

        assertNotEquals("hola",list2.get(1));
        assertNotEquals("chau",list2.get(2));

        assertNull(list.get(3));         // el index sobrepasa la lista
        assertNull(list.get(6));

        assertNull(list2.get(-2));       // numeros negativos
        assertNull(list2.get(-7));
    }

    @Test
    void setTest(){
        List<Integer> list = new LinkedList<>();

        assertNull(list.set(1,10));       // lista vacia

        list.add(1);
        list.add(2);
        list.add(3);

        assertEquals(1,list.set(0,10));
        assertEquals(10,list.get(0));

        assertNotEquals(1,list.set(3,100));   // index excede la lista
        assertNotEquals(3,list.set(5,10));

        assertNotEquals(1,list.set(-1,8));     //indice negativo
        assertNotEquals(2,list.set(-3,20));

        assertEquals(2,list.set(1,2));    // no pasa nada si se cambia por el mismo valor
        assertEquals(3,list.set(2,3));

        List<String> list2 = new LinkedList<>();
        assertNull(list2.set(1,"five"));          // lista vacia

        list2.add("one");
        list2.add("two");
        list2.add("tree");

        assertEquals("one",list2.set(0,"uno"));
    }

    @Test
    void indexOfTest(){
        List<Integer> list = new LinkedList<>();
        assertEquals(-1, list.indexOf(2));  // lista vacia

        list.add(1);
        list.add(5);  // index = 1
        list.add(3);  //         2
        list.add(5);  // index = 3
        list.add(3);  //         4

        assertEquals(1,list.indexOf(5));
        assertEquals(2,list.indexOf(3));  // normal

        assertEquals(-1, list.indexOf(2));  // no hay objeto
        assertEquals(-1, list.indexOf(10));

        assertEquals(-1, list.indexOf("cadena"));

        List<String> list2 = new LinkedList<>();
        assertEquals(-1, list.indexOf("1"));  // lista vacia
        list2.add("1");
        list2.add("4");
        list2.add("10");
        list2.add("1");
        list2.add("10");

        assertEquals(2,list2.indexOf("10"));  // normal
        assertEquals(0,list2.indexOf("1"));

        assertEquals(-1, list2.indexOf("11"));  // no hay objeto
        assertEquals(-1, list2.indexOf("13"));

        assertEquals(-1, list2.indexOf(1));
    }

    @Test
    void lastIndexOf(){
        List<Integer> list = new LinkedList<>();
        assertEquals(-1, list.lastIndexOf(3));  // lista vacia

        list.add(1);
        list.add(5);  // index = 1
        list.add(3);  //         2
        list.add(5);  // index = 3
        list.add(3);  //         4

        assertEquals(3,list.lastIndexOf(5));
        assertEquals(4,list.lastIndexOf(3));  // normal

        assertEquals(-1, list.lastIndexOf(2));  // no hay objeto
        assertEquals(-1, list.lastIndexOf(10));

        assertEquals(-1, list.lastIndexOf("cadena"));

        List<String> list2 = new LinkedList<>();
        assertEquals(-1, list.lastIndexOf("1"));  // lista vacia
        list2.add("1");
        list2.add("4");
        list2.add("10");
        list2.add("1");
        list2.add("10");

        assertEquals(4,list2.lastIndexOf("10"));  // normal
        assertEquals(3,list2.lastIndexOf("1"));

        assertEquals(-1, list2.lastIndexOf("11"));  // no hay objeto
        assertEquals(-1, list2.lastIndexOf("13"));

        assertEquals(-1, list2.lastIndexOf(1));
    }

    @Test
    void iteratorTest(){
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        int count = 0;
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()){
            Integer i = iterator.next();

            assertEquals(list.get(count),i);
            count++;
        }

        List<String> listString = new LinkedList<>();
        listString.add("HOLA");
        listString.add("A TODO");
        listString.add("EL MUNDO");

        int count2 = 0;
        Iterator<String> iteratorString = listString.iterator();
        while (iteratorString.hasNext()){
            String i = iteratorString.next();

            assertEquals(listString.get(count2),i);
            count2++;
        }
    }

    @Test
    void removeTest(){
        // ------    por indice ---------
        List<Integer> list = new LinkedList<>();
        assertNull(list.remove(1));   // lista vacia
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        assertEquals(4,list.size());
        assertEquals(2,list.remove(1));  // normal
        assertEquals(3,list.size());
        assertEquals(4,list.remove(2));
        assertEquals(2,list.size());

        assertNull(list.remove(5));  // index excede la lista
        assertNull(list.remove(10));

        assertNull(list.remove(-1));  // index negativo
        assertNull(list.remove(-10));

        //---------- por Objeto -----------

        List<String> list2 = new LinkedList<>();
        assertNull(list2.remove(1));   // lista vacia
        list2.add("1");
        list2.add("2");
        list2.add("3");
        list2.add("4");

        assertEquals(4,list2.size());
        assertTrue(list2.remove("1"));  // normal
        assertEquals(3,list2.size());
        assertTrue(list2.remove("2"));
        assertEquals(2,list2.size());

        assertFalse(list2.remove("10"));  //no hay objeto
        assertFalse(list2.remove("hola"));

    }

}
