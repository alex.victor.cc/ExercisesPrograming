package tareas;

import stack.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StackTest {
    @Test
    void push(){
        Stack<Integer> stack = new Stack<>();

        stack.push(1);

        assertEquals(1,stack.size());
        assertEquals("{1}",stack.toString());

        stack.push(2);
        assertEquals(2,stack.size());
        assertEquals("{1,2}",stack.toString());

        stack.push(3);
        assertEquals(3,stack.size());
        assertEquals("{1,2,3}",stack.toString());

        stack.push(4);

        assertEquals(4,stack.size());
        assertEquals("{1,2,3,4}",stack.toString());

        //  si el valor es null
        stack.push(null);
        assertEquals(5,stack.size());
        assertEquals("{1,2,3,4,null}",stack.toString());

        Stack<String> stackS = new Stack<>();

        stackS.push("Hola");
        assertEquals(1,stackS.size());
        assertEquals("{Hola}",stackS.toString());

        stackS.push("a");
        assertEquals(2,stackS.size());
        assertEquals("{Hola,a}",stackS.toString());

        stackS.push("todo");
        assertEquals(3,stackS.size());
        assertEquals("{Hola,a,todo}",stackS.toString());

        stackS.push("el mundo");

        assertEquals(4,stackS.size());
        assertEquals("{Hola,a,todo,el mundo}",stackS.toString());

        // si el valor es null
        stackS.push(null);
        assertEquals(5,stackS.size());
        assertEquals("{Hola,a,todo,el mundo,null}",stackS.toString());

    }

    @Test
    void pop(){
        Stack<Integer> stack = new Stack<>();
        //no hace nada con una lista vacia
        stack.pop();
        assertEquals(0,stack.size());

        stack.push(1);
        stack.push(null);
        stack.push(3);
        stack.push(4);

        assertEquals(4,stack.size());
        assertEquals("{1,null,3,4}",stack.toString());

        stack.pop();
        assertEquals(3,stack.size());
        assertEquals("{1,null,3}",stack.toString());

        stack.pop();
        assertEquals(2,stack.size());
        assertEquals("{1,null}",stack.toString());

        // con un valor recien ingresado
        stack.push(100);
        assertEquals(3,stack.size());
        assertEquals("{1,null,100}",stack.toString());
        stack.pop();
        assertEquals(2,stack.size());
        assertEquals("{1,null}",stack.toString());

        stack.pop();
        assertEquals(1,stack.size());
        assertEquals("{1}",stack.toString());

        stack.pop();
        assertEquals(0,stack.size());
        assertEquals("{null}",stack.toString());

        Stack<String> stackS = new Stack<>();
        //no hace nada con una lista vacia
        stackS.pop();
        assertEquals(0,stackS.size());

        stackS.push("Hola");
        stackS.push(null);
        stackS.push("a todo");
        stackS.push("el mundo");

        assertEquals(4,stackS.size());
        assertEquals("{Hola,null,a todo,el mundo}",stackS.toString());

        stackS.pop();
        assertEquals(3,stackS.size());
        assertEquals("{Hola,null,a todo}",stackS.toString());

        stackS.pop();
        assertEquals(2,stackS.size());
        assertEquals("{Hola,null}",stackS.toString());

        // con un valor recien ingresado
        stackS.push("zzzzzzzz");
        assertEquals(3,stackS.size());
        assertEquals("{Hola,null,zzzzzzzz}",stackS.toString());
        stackS.pop();
        assertEquals(2,stackS.size());
        assertEquals("{Hola,null}",stackS.toString());

        stackS.pop();
        assertEquals(1,stackS.size());
        assertEquals("{Hola}",stackS.toString());

        stackS.pop();
        assertEquals(0,stackS.size());
        assertEquals("{null}",stackS.toString());
    }

}
