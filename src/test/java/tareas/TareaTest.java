package tareas;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TareaTest {
    @Test
    void canCreateAppMain(){
        Tarea test = new Tarea();
        int[] array = {1,2,3,4,5};
        int rotateNumber = 7;
        int[] expected = {4,5,1,2,3};
        assertArrayEquals(expected,test.rotate(array,rotateNumber));

        int[] current = test.rotate(array,rotateNumber);
        assertEquals(4,current[0]);

        rotateNumber = 0;
        assertEquals(array,test.rotate(array,rotateNumber));

        int[] arrayTwo = {1,2,3};
        rotateNumber = -5;
        int[] expectedTwo = {3,1,2};
        assertArrayEquals(expectedTwo,test.rotate(arrayTwo,rotateNumber));

        rotateNumber = 11;
        int[] expectedTree = {5,1,2,3,4};
        assertArrayEquals(expectedTree,test.rotate(array,rotateNumber));

        rotateNumber = 12478;
        int[] expectedFour = {3,4,5,1,2};
        assertArrayEquals(expectedFour,test.rotate(array,rotateNumber));
    }
}
