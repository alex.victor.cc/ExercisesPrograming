package CircularLinkedLIst;

public class CircularLinkedList<T> implements List<T> {
    private Node<T> head;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean add(T data) {
        Node<T> newNode = new Node<>(data);
        newNode.setNext(head);
        Node<T> lastNode;
        if(isEmpty()){
            head = newNode;
        }
        else {
            lastNode = getLastNode();
            lastNode.setNext(newNode);
        }
        size ++;
        return true;
    }

    @Override
    public boolean remove(T data) {
        if(isEmpty()){
            return false;
        }else {
            Node<T> current = head;
            Node<T> previous = getLastNode();
            for (int i = 0; i < size; i++,current = current.getNext(),previous = previous.getNext()){
                if (current.getData() == data) {
                    if (head.equals(current)){
                        head = head.getNext();
                        previous.setNext(head);

                    }else{
                        previous.setNext(current.getNext());
                    }
                    size--;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index < 0)
            return null;
        else {
            Node<T> node = head;
            for (int i = 0; i < index ; i++, node = node.getNext());

            return node.getData();
        }
    }

    public static List<Integer> dummyList(int s) {
        CircularLinkedList<Integer> l = new CircularLinkedList<>();
        l.size = s;
        l.head = new Node<>(l.size);
        dummyNode(l.head, --s, null);
        return l;
    }
    private static Node<Integer> dummyNode(Node<Integer> n, int d, Node<Integer> z) {
        z = z != null ? z : n;
        if (d <= 0) { n.setNext(z); return n;}
        else n.setNext(dummyNode(new Node<>(d), d -1, z));
        return  n;
    }

    private Node<T> getLastNode(){
        Node<T> node = head;
        for (int i = 1; i < size; i++) {
            node = node.getNext();
        }
        return node;
    }

    public String toString() {
        return "(" + size + ")" + " head = " + head.toString(head);
    }
}
