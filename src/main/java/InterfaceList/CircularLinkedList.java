package InterfaceList;

public class CircularLinkedList<T extends Comparable<T>> implements List<T>{
    private Node<T> head;
    private int size;


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean add(T date) {
        if(date == null){
            return false;
        }
        Node<T> node = new Node<>(date);
        if(isEmpty()){
            head = node;
            head.setNext(head);
        }
        else {
            Node<T> last = getLast();
            last.setNext(node);
        }
        size++;
        Node<T> lastNode = getLast();
        lastNode.setNext(head);
        return true;
    }

    @Override
    public boolean remove(T date) {
        if(isEmpty()){
            return false;
        }
        Node<T> node = head;
        Node<T> previous = getLast();
        for (int i = 0; i < size; i++,node = node.getNext(),previous = previous.getNext()) {
            if(node.getDate().equals(date)){
                if(head == node){
                    if(size == 1){
                        head = null;
                    }else{
                        head = head.getNext();
                        previous.setNext(head);
                    }

                }else{
                    previous.setNext(node.getNext());
                }
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public T get(int index) {
        if(isEmpty() || index < 0 ){
            return null;
        }
        Node<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.getNext();
        }
        return node.getDate();
    }

    @Override
    public void selectionSort() {
        if(!isEmpty()){
            Node<T> node = head;
            Node<T> previousNode = getLast();

            for (int i = 0; i < size - 1; i++) {

                Node<T> min = node.getNext();
                Node<T> previousMin = node;
                while (!(node.getDate().compareTo(min.getDate()) > 0) ){
                    min = min.getNext();
                    previousMin = previousMin.getNext();
                }

                Node<T> aux = node.getNext();
                node.setNext(min.getNext());
                previousMin.setNext(node);
                min.setNext(aux);
                previousNode.setNext(min);

                if(i == 0){
                    head = min;
                }
                node = min.getNext();
                previousNode = min;
            }
            Node<T> last = getLast();
            last.setNext(head);
        }
    }

    @Override
    public void bubbleSort() {
        if(!isEmpty()){
            boolean sorted;
            do {
                sorted = true;
                Node<T> node = head;
                Node<T> next = head.getNext();
                Node<T> previousNode = getLast();
                for (int i = 0; i < size - 1; i++) {
                    if (node.getDate().compareTo(next.getDate()) > 0) {
                        node.setNext(next.getNext());
                        next.setNext(node);

                        if(i == 0){
                            head = next;
                            previousNode = head;

                        }
                        else {
                            previousNode.setNext(next);
                            previousNode = previousNode.getNext();
                        }
                        next = node.getNext();
                        sorted  = false;
                    }else{
                        node = node.getNext();
                        next = next.getNext();
                        previousNode = previousNode.getNext();
                    }
                }
                Node<T> last = getLast();
                last.setNext(head);

            } while (!sorted);
        }

    }

    @Override
    public void reverse(){
       if(!isEmpty() || size == 1) {
            Node<T> reverse = getLast();
            Node<T> aux = getLast();
            for (int i = 1; i < size; i++) {
                Node<T> node = head;
                while (!(node.getNext().getNext() == head)) {
                    node = node.getNext();
                }
                node.setNext(head);
                reverse.setNext(node);
                reverse = reverse.getNext();
            }
            head = aux;
            Node<T> last = getLast();
            last.setNext(head);
        }
    }

    @Override
    public void mergeSort() {

    }

    private Node<T> getLast(){
        Node<T> node = head;
        for (int i = 1; i < size; i++) {
            node = node.getNext();
        }
        return node;
    }

    public String toString() {
        return "(" + size + ")" + " head = " + head.toString(head);
    }


    public static List<Integer> dummyList(int s) {
        CircularLinkedList<Integer> l = new CircularLinkedList<>();
        l.size = s;
        l.head = new Node<>(l.size);
        dummyNode(l.head, --s, null);
        return l;
    }
    private static Node<Integer> dummyNode(Node<Integer> n, int d, Node<Integer> z) {
        z = z != null ? z : n;
        if (d <= 0) { n.setNext(z); return n;}
        else n.setNext(dummyNode(new Node<>(d), d -1, z));
        return  n;
    }
}
