package InterfaceList;

public class Node<T extends Comparable<T>> {
    private Node<T> next;

    private final T date;

    public Node(T date ){
        this.date = date;
        this.next = null;

    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public T getDate() {
        return date;
    }

    public String toString(Node<T> head) {
        if(next.equals(head)){
            return "{ data = " + date + ", next -> head }";
        }
        return "{ data = " + date + ", next ->" +next.toString(head)+ "}";
    }
}
