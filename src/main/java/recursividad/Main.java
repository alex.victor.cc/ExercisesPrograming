package recursividad;

import recursividad.recursividad.*;

public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        //LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Descripcion:

        // El metodo whileTrue() se ejecuta correctamente , pero
        // lo que contiene es una llamada a este mismo metodo,
        // el metodo se ejcuta de nuevo y se vuelve a llamar
        // asi mismo, se ejecuta de nuevo y asi se repite.
        // Como no hay mas codigo en ese metodo , no se le indica
        // al metodo en que momento parar , por eso se repite de
        // manera infinita , el compilador se da cuenta de eso,
        // es por eso que sale el error StackOverflowError

        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        //Infinito.whileTrue();

        

        // descomentar la llamda a: Contador.contarHasta(), pasnado un humer entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion:.................

        // al metodo contarHasta()  se le pasa un numero entero
        // lo primero que hace con este numero es imprimirlo en la consola
        // luego pasa por un condicional que especifica que si este numero
        // es mayor que cero entonces volvera a llamr al mismo metodo, con
        // la diferencia de que esta otra llamda del metodo le pasa
        // el numero que le dieron reducido en 1, asi el metodo se ira
        // repitiendo mientras se cumpla con la condicional de que sea mayor que 0
        // asi el numero se ira reduciendo con cada llamada al metodo
        // cuando el numero llegue a cero simplemente no entrara en la condicional
        // y ahi termina el metodo, por lo que el condicional nos sirve para
        // que el metpdp no se repita infinitas veces
        Contador.contarHasta(5);

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));
    }
}
