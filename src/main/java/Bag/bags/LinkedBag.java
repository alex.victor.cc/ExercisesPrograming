package Bag.bags;

public class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }
    public String toString() {
        return "" + root;
    }

    @Override
    public void selectionSort() {
        for (int i = 0; i < size - 1; i++) {
            int indexMin = i;
            for (int j = i + 1; j < size; j++) {
                if (get(i).getData().compareTo(get(j).getData()) > 0) {
                    indexMin = j;
                }
            }
            if(indexMin != i){
                Node<T> current = get(i), nextCurrent = get(i + 1);
                Node<T> min = get(indexMin), prevMin = get(indexMin - 1), nextMin = get(indexMin +1);
                if(i == 0){
                    root = min;
                }else {
                    Node<T> prev = get(i - 1);
                    prev.setNext(min);
                }
                if((i + 1) == indexMin){
                    current.setNext(nextMin);
                    min.setNext(current);
                }else {
                    min.setNext(nextCurrent);
                    prevMin.setNext(current);
                    current.setNext(nextMin);
                }
            }
        }
    }


    @Override
    public void bubbleSort() {
        boolean sorted;
        do {
            sorted = true;
            for (int i = 0; i < size - 1; i++) {
                if (get(i).getData().compareTo(get(i + 1).getData()) > 0) {
                    Node<T> current = get(i);
                    Node<T> min = get((i + 1)), nextMin = get(i + 2);
                    if(i == 0){
                        root = min;
                    }else {
                        Node<T> prev = get(i - 1);
                        prev.setNext(min);
                    }
                    current.setNext(nextMin);
                    min.setNext(current);
                    sorted = false;
                }
            }

        } while (!sorted);
    }

    private Node<T> get(int index){
        Node<T> node = root;
        for (int i = 0; i < index; i++) {
            node = node.getNext();
        }
        return node;
    }

    @Override
    public void xchange(int x, int y) {
        Node<T> nodeX = root, previoX = null, nodeY = root, previoY = null;
        for (int i = 0; i < x; i++) {
            previoX = nodeX;
            nodeX = nodeX.getNext();
        }
        for (int i = 0; i < y; i++) {
            previoY = nodeY;
            nodeY = nodeY.getNext();
        }
        Node<T> nextY = nodeY.getNext(), nextX = nodeX.getNext();
        previoX.setNext(nodeY);
        nodeY.setNext(nextX);
        previoY.setNext(nodeX);
        nodeX.setNext(nextY);
    }
}
