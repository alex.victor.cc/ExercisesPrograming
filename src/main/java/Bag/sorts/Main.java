package Bag.sorts;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

public class Main implements Comparable<Main> {
    public static void main(String[] args) {
        //doSmallTest();
        doLargeTest();
    }


    public static void doSmallTest() {
        String[] numsStr = {"4","5","2","1","3"};
        int[] numsInt = Arrays.stream(numsStr).mapToInt(Integer::parseInt).toArray();

        String[] numsStrClone = numsStr.clone();

        int[] numsIntClone = numsInt.clone();

        Main[] mains = new Main[5];
        Sort.selection(mains);

        System.out.println("selection");
        Instant inst1 = Instant.now();
        Sort.selection(numsStrClone);
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
        System.out.println(Arrays.toString(numsStr));
        System.out.println(Arrays.toString(numsStrClone));

        Sort.selectionInt(numsIntClone);
        System.out.println(Arrays.toString(numsInt));
        System.out.println(Arrays.toString(numsIntClone));

        numsStrClone = numsStr.clone();

        numsIntClone = numsInt.clone();


        System.out.println("bubbles");
        inst1 = Instant.now();
        Sort.bubble(numsStrClone);
        inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
        System.out.println(Arrays.toString(numsStr));
        System.out.println(Arrays.toString(numsStrClone));

        Sort.bubbleInt(numsIntClone);
        System.out.println(Arrays.toString(numsInt));
        System.out.println(Arrays.toString(numsIntClone));
    }

    public static void doLargeTest() {
        int len = 30_000;
        String[] numsStr = new String[len];
        for (int i = 0; i < numsStr.length; i++) {
            numsStr[i] = Integer.toString((int)(Math.random() * len));
        }

        String[] numsStrClone = numsStr.clone();

        System.out.println("selection");
        Instant inst1 = Instant.now();
        Sort.selection(numsStrClone);
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());

        //System.out.println(Arrays.toString(numsStr));
        //System.out.println(Arrays.toString(numsStrClone));

        numsStrClone = numsStr.clone();

        System.out.println("bubbles");
        inst1 = Instant.now();
        Sort.bubble(numsStrClone);
        inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());


        //System.out.println(Arrays.toString(numsStr));
        //System.out.println(Arrays.toString(numsStrClone));
    }

    @Override
    public int compareTo(Main o) {
        return 0;
    }
}
