package Clase30ago;

public class Main {
    public static void main(String[] args){
        Tail<Integer> tail = new Tail<>();

        tail.add(1);
        tail.add(2);
        tail.add(3);
        tail.add(4);

        System.out.println(tail);

        System.out.println(tail.remover()); // 1
        System.out.println(tail.remover()); // 2
        System.out.println(tail.remover()); // 3
        System.out.println(tail.remover()); // 4
        tail.add(5);
        System.out.println(tail.remover()); // 5

        // Ejercicio sep 1
        Tail<Integer> tail1 = new Tail<>();
        tail1.add(1);tail1.add(3);tail1.add(5);

        Tail<Integer> tail2 = new Tail<>();
        tail2.add(2);tail2.add(4);tail2.add(6);

        Tail<Integer> tail3 = new Tail<>();

        while (true){
            Integer num1 = tail1.remover();
            Integer num2 = tail2.remover();
            if (num1 != null){
                tail3.add(num1);
            }
            if ( num2 != null){
                tail3.add(num2);
            }
            if ( num1 == null && num2 == null){
                break;
            }
        }

        System.out.println(tail3);
    }
}
