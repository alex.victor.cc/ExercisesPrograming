package Clase30ago;

import javax.print.attribute.standard.SheetCollate;

public class Tail<T> {
    private Node<T> tail;
    private Node<T> head;

    public Tail(){
        this.tail = null;
        this.head = null;
    }

    public void add(T date){
        Node<T> node = new Node<>(date);

        if(head == null){
            head = node;
            tail = node;
        }else {
            Node<T> aux = tail;
            tail = node;
            tail.setNext(aux);
        }

    }

    public T remover(){
        if(tail == null){
            return null;
        }
        T date = head.getData();
        Node<T> node = tail;
        if(tail.equals(head)){
            head = null;
            tail = null;
        }else {
            while (!node.getNext().equals(head)){
                node = node.getNext();
            }
            head = node;
            head.setNext(null);
        }
        return date;
    }

    public String toString() {
        return " tail = " + tail.toString(tail);
    }
}
