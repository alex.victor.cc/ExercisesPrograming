package tareas;

public class Tarea {

    /**
     * This method has the function of rotating the positions of the numbers in an array.
     *
     * moves to the right when the number is positive,
     * to the left when it is negative,
     * and returns the same order when it is zero.
     *
     * @param arrayNumbers a list of numbers.
     * @param rotateNumber number of positions to rotate.
     * @return a new arrangement with the order changed according to the number you entered.
     */
    public int[] rotate(int[] arrayNumbers, int rotateNumber){

        int arrayLength = arrayNumbers.length;
        int[] finalArray = new int[arrayLength];

        int numRotations = Math.abs(rotateNumber % arrayLength);
        int positionsDifference = arrayLength - numRotations ;

        int firsPartStart = 0;
        int secondPartStart = 0;

        if(rotateNumber == 0){
            finalArray = arrayNumbers;
        }
        else{
            if(rotateNumber > 0){
                firsPartStart = positionsDifference;
                secondPartStart = numRotations;
            }
            if(rotateNumber < 0){
                firsPartStart = numRotations;
                secondPartStart = positionsDifference;
            }

            for(int i = firsPartStart; i < arrayLength ; i ++){
                finalArray[i - firsPartStart] = arrayNumbers[i];
            }
            for (int i = 0; i < firsPartStart; i++) {
                finalArray[i + secondPartStart] = arrayNumbers[i];
            }
        }

        return finalArray;
    }
}
