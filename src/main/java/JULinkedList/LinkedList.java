package JULinkedList;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList<T> implements List<T> {
    private int length;
    private Node<T> root;

    @Override
    public int size() {
        return length;
    }

    @Override
    public void clear() {
        root = null;
        length = 0;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public boolean contains(Object o) {
        if(isEmpty()){
            return false;
        }
        for (Node<T> node = root; node != null; node = node.getNext()) {
            if(node.getDate().equals(o)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> node = root;
            @Override
            public boolean hasNext() {
                if(isEmpty()){
                    return false;
                }
                return node != null;
            }

            @Override
            public T next() {
                Node<T> current = node;
                node = node.getNext();
                return current.getDate();
            }
        };
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index >= length || index < 0){
            return null;
        }
        Node<T> node = root;
        for (int i = 0; i < index; i++){
            node = node.getNext();
        }
        return node.getDate();
    }

    @Override
    public T set(int index, T element) {
        T datePrevious = get(index);
        if(isEmpty() || index >= length || index < 0){
            return null;
        }
        Node<T> node = root;
        for (int i = 0; i < index; i++){
            node = node.getNext();
        }
        node.setDate(element);
        return datePrevious;
    }

    @Override
    public boolean add(T t){
        Node<T> node = new Node<>(t);
        if (isEmpty()) root = node;
        else {
            Node<T> ultimo = getNode(length - 1);
            assert ultimo != null;
            ultimo.setNext(node);
        }
        length++;
        return true;
    }

    @Override
    public T remove(int index) {
        T dateRemoved;
        if(isEmpty() || index >= length || index < 0){
            return null;
        }
        if(index == 0){
            dateRemoved = root.getDate();
            root = root.getNext();
        }else {
            Node<T> node = root;
            Node<T> previous = null;
            for (int i = 0; i < index; i++) {
                previous = node;
                node = node.getNext();
            }
            dateRemoved = node.getDate();
            previous.setNext(node.getNext());
        }
        length --;
        return dateRemoved;
    }

    @Override
    public boolean remove(Object o) {
        Node<T> previous = root;
        for (Node<T> actual = root; actual != null; previous = actual, actual = actual.getNext()) {
            if (actual.getDate().equals(o)) {
                if (root == actual)
                    root = actual.getNext();
                else
                    previous.setNext(actual.getNext());
                length--;
                return true;
            }
        }
        return false;
    }

    @Override
    public int indexOf(Object o) {
        if(isEmpty()){
            return -1;
        }
        Node<T> node = root;
        for (int i = 0; i < length; i++,node = node.getNext()) {
            if(node.getDate().equals(o)){
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int lastIndex = -1;

        if(isEmpty()){
            return lastIndex;
        }

        Node<T> node = root;
        for (int i = 0; i < length; i++,node = node.getNext()) {
            if(node.getDate().equals(o)){
                lastIndex = i;
            }
        }
        return lastIndex;
    }

    private Node<T> getNode(int index){
        if (isEmpty() || index >= length)
            return null;
        Node<T> node = root;
        for (int i = 0; i < index; i++){
            node = node.getNext();
        }
        return node;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }




    @Override
    public void add(int index, T element) {

    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }
}
