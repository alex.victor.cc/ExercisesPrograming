package clase2907;

import java.util.Iterator;
import java.util.List;

interface Attendee{
    void join();
}
class Student implements Attendee {
    String name;
    int totalGrade;
    public Student(String name) {
        this.name = name;
    }
    public void join() {
        System.out.println(name + ", joined");
    }

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) return true;

        if(!(obj instanceof Student)) return false;

        Student st = (Student)obj;
        return this.totalGrade == st.totalGrade &&
                this.name.equals(st.name);
    }
}
class Teacher implements Attendee {
    String name;
    public Teacher(String name) {
        this.name = name;
    }
    public void join() {}
}
class RegisterAttendees {
    private Attendee[] attendees = new Attendee[10];

    // unicos, no se repiten
    public void Register(Attendee attendee) {
        for (int i = 0; i < attendees.length; i++) {
            if (attendees[i] == null) {
                attendees[i] = attendee;
                break;
            } else if (attendee.equals(attendees[i])) {
                break;
            }
        }
    }
    public int getTotalAttendees() {
        int total = 0;
        for (int i = 0; i < attendees.length; i++) {
            if (attendees[i] == null)
                break;

            total++;
        }
        return total;
    }
}
public class Main {
    public static void main(String[] args) {
        List l1;
        Iterator i1;
        Iterable ii;
        Attendee a1 = new Student(new String("John"));
        Attendee t1 = new Teacher(new String("John"));
        Attendee a2 = new Student(new String("John"));
        System.out.println(a1 == a2);
        System.out.println(a1.equals(a2));
        RegisterAttendees r = new RegisterAttendees();
        r.Register(a1);
        r.Register(t1);
        r.Register(a2);
        // total should be 2
        System.out.println(r.getTotalAttendees() == 2);
        System.out.println(r.getTotalAttendees());
    }
}
