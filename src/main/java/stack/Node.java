package stack;

public class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data;}
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {
        if(next == null){
            return "" + data;
        }
        return next.toString() + "," + data;
    }
}