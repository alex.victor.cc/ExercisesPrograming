package stack;

public class Stack<T extends Comparable<T>> {
    private Node<T> head = null;
    private int size = 0;


    public void push(T date ){
        Node<T> node = new Node<>(date);
        node.setNext(head);
        head = node;
        size++;
    }

    public void pop(){
        if(head != null){
            head = head.getNext();
            size--;
        }
    }

    public int size(){
        return size;
    }

    public String toString() {
        return "{" + head + "}";
    }
}
