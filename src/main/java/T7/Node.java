package T7;

public class Node<T> {
    public T data;
    public Node<T> next;

    public Node(T data) {
        this.data = data;
        this.next = null;
    }

    public Node(T data, Node<T> next) {
        this.data = data;
        this.next = next;
    }


    public T getNth(Node<T> n, int index){
        if(index >= size(n) || n == null){
            return null;
        }
        Node<T> node = n;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node.data;
    }

    public  Node<T> append(Node<T> listA, Node<T> listB) {
        if(listA != null && listB != null){
            Node<T> node = listA;
            while (node.next != null){
                node = node.next;
            }
            node.next = listB;
        }else {
            if(listA == null){
                return listB;
            }
        }
        return listA;
    }

    public String toString() {
        return "" + data + next;
    }
    
    private int size(Node<T> node){
        int size = 0;
        Node<T> nod = node;
        while (nod != null){
            size++;
            nod = nod.next;
        }
        return size;
    }
}
