package Game;

public class Entity {
    private String identifier ;
    private int row;
    private int column;

    /**
     * This method generate entities.
     *
     * Entities can be either players or objects.
     *
     * @param identifier the identifier inside the board
     * @param row the row where the object is located
     * @param column the column where the object is located
     */
    public Entity(String identifier, int row , int column){
        this.identifier = identifier;
        this.row = row;
        this.column = column;
    }

    /**
     * This method performs a one-square move in one direction.
     *
     * 1.- Up
     * 2.- Down
     * 3.- Right
     * 4.- Left
     *
     * @param option a number representing an action
     */
    public void move(int option){
        if(option == 1 ){
            row = row - 1;
        }
        if(option == 2 ){
            row = row + 1;
        }
        if(option == 3 ){
            column = column + 1;
        }
        if(option == 4){
            column = column - 1;
        }
    }

    public String getIdentifier() {
        return identifier;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }
}
