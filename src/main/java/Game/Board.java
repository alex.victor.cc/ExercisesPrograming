package Game;

public class Board {
    private final String[][] board ;
    private String[][] currentBoard;
    public String emptySpace = "-";

    public Board(int numRows, int numColumns){
        this.board = new String[numRows][numColumns];
    }

    /**
     * This method generate a new board.
     */
    public void generateBoard(){
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = emptySpace;
                if(i == (board.length - 1) && j == (board[i].length - 1)){
                    board[i][j] = " ";
                }
            }
        }
    }

    /**
     * This method generates what will be displayed on the screen representing the board.
     * @return a graphic board.
     */
    public String generateGraph(){
        String graphBoard = "";
        for(int i = 0; i < currentBoard.length; i++) {
            graphBoard += "|";
            for (int j = 0; j < currentBoard[i].length; j++) {
                graphBoard += " ";
                graphBoard += currentBoard[i][j];
            }
            graphBoard += " |" + "\n";
        }
        return graphBoard;
    }

    /**
     * This method draws the entities that are on the board.
     *
     * @param entities list of map entities
     */
    public void updateBoard(Entity[] entities){
        currentBoard = board;
        for (Entity entity : entities) {
            int row = entity.getRow();
            int column = entity.getColumn();
            currentBoard[row][column] = entity.getIdentifier();
        }
    }

    public String[][] getCurrentBoard() {
        return currentBoard;
    }
}
