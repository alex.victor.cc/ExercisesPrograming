package Game;

import java.util.Scanner;

public class Manager {
    private Board board ;
    private int obstacles;

    private Entity[] entities;
    private Entity[] players;
    private int numPlayers;

    private int numRows = 10;
    private int numColumns = 10;

    private int[] occupiedRows;
    private int[] occupiedColumns;

    private Scanner scan ;
    private int option;
    private int row;
    private int column;

    public Manager(int obstacles,int numPlayers){
        this.board = new Board(numRows,numColumns);
        this.scan = new Scanner(System.in);
        this.obstacles = obstacles;
        this.numPlayers = numPlayers;
        this.entities = new Entity[obstacles + numPlayers];
        this.occupiedRows = new int[obstacles + numPlayers];
        this.occupiedColumns = new int[obstacles + numPlayers];
        this.players = new Entity[numPlayers];

    }

    /**
     * This method organizes the methods for the game to run properly.
     */
    public void startGame(){
        board.generateBoard();
        generateRandomCoords();
        assignCoords();
        board.updateBoard(entities);
        String[][] lastPosition = board.getCurrentBoard();
        while(lastPosition[numRows - 1][numColumns - 1].equals(" ")){// mientras el ultimo siga vacio
            System.out.println(board.generateGraph());
            inputMovement();
            takeAction();
            board.updateBoard(entities);
        }
    }

    /**
     * This method assigns the coordinates to the entities.
     */
    public void assignCoords(){
        for (int i = 0; i < obstacles; i++) {
            Entity obstacle = new Entity("#",occupiedRows[i],occupiedColumns[i]);
            entities[i] = obstacle;
        }
        for (int i = obstacles; i < entities.length; i++) {
            Entity player = new Entity("A",occupiedRows[i],occupiedColumns[i]);
            entities[i] = player;
            players[0] = player;
        }

    }

    /**
     * This method generate random coordinates.
     *
     * This random coordinates are different from each other and different from the final square of the board.
     *
     */
    private void generateRandomCoords(){
        for (int i = 0; i < entities.length; i++) {
            boolean coordsIsOccupied;
            do {
                generateCoords();
                coordsIsOccupied = validatorCoords(row, column);
            } while (coordsIsOccupied);
            occupiedRows[i] = row;
            occupiedColumns[i] = column;
        }
    }

    /**
     * This method is in charge of validating if the generated coordinates are not repeated.
     *
     * @param row row to be validated
     * @param column column to be validated
     * @return is valid coordinate
     */
    private boolean validatorCoords(int row,int column){
        boolean validator = false;

        if( (row == ( numRows - 1) && column == (numColumns - 1) ) ){
            validator = true;
        }

        for (int i = 0; i < entities.length; i++) {
            if (occupiedRows[i] == row && occupiedColumns[i] == column) {
                validator = true;
                break;
            }
        }
        return validator;
    }

    /**
     * This method generates two random coordinates.
     */
    private void generateCoords(){
        row = (int) (Math.random()*(numRows));
        column = (int) (Math.random()*(numColumns));
    }

    /**
     * This method instructs the player to enter a movement.
     */
    public void inputMovement(){
        System.out.print("1 .- UP" + "\n" +
                "2 .- DOWN" + "\n" +
                "3 .- RIGHT" + "\n" +
                "4 .- LEF" + "\n" +
                "input a option: ");
        option = scan.nextInt();
    }

    /**
     * This method executes the chosen option.
     */
    private void takeAction(){
        players[0].move(option);
    }
}
